import uuid
import random
import json
import datetime
from django.http import JsonResponse
from django.db.models import Q
from .models import User, Games


def games(request):
    game = Games.objects.filter(Q(player1=request.session['id']) | Q(
        player2=request.session['id'])).order_by('-updated')[:4]

    myid = User.objects.get(id=request.session['id'])

    def extract(game):
        if game.player2:
            player2 = game.player2.username
        else:
            player2 = ''

        winner = ''
        if game.winner:
            winner = game.winner.id

        finished = False

        if game.board.count(' ') == 0 and winner == '':
            finished = True

        return {
            'id': game.id,
            'player1': game.player1.username,
            'player2': player2,
            'lastupdated': game.updated,
            'winner':  winner,
            'completed': finished
        }

    g = map(extract, game)

    return JsonResponse({'games': list(g),
                         'user': myid.id})


def userinfo(request):
    u = User.objects.get(id=request.session['id'])
    if request.method == 'POST':
        body = json.loads(request.body.decode('utf-8'))
        if body.get('nickname') != '':
            u.username = body.get('nickname')
            u.save()

    totalgames = Games.objects.filter(Q(player1=request.session['id']) | Q(
        player2=request.session['id'])).count()

    return JsonResponse({
        'id': u.id,
        'win': u.win,
        'lose': u.lose,
        'draw': u.draw,
        'username': u.username,
        'total': totalgames
    })


def user(request):

    if 'id' in request.session:
        u = User.objects.get(id=request.session['id'])
    else:
        ID = uuid.uuid4()
        request.session['id'] = str(ID)
        u = User(id=ID)
        u.save()

    return JsonResponse({
        'id': u.id,
        'win': u.win,
        'lose': u.lose,
        'draw': u.draw,
        'username': u.username
    })


def state(request, id, win='', valid=''):
    g = Games.objects.get(id=id)
    u = User.objects.get(id=request.session['id'])

    if g.player1.id == request.session['id']:
        counter = 'X'
    else:
        counter = 'O'

    if win == '':
        win = rules(g.board.split(','), win)

    if g.winner != None:
        winner = g.winner.id
    else:
        winner = ''

    return JsonResponse({
        'username': u.username,
        'counter': counter,
        'board': g.board,
        'player': g.turn,
        'win': win,
        'valid': valid,
        'gameid': g.id,
        'winner': winner
    })


def setgame(request):
    body = json.loads(request.body.decode('utf-8'))

    gameid = uuid.uuid4()

    g = Games(
        id=str(gameid),
        player1=User.objects.get(id=request.session['id']),
        turn='X',
        board=' , , , , , , , , ',
        created=datetime.datetime.now(),
        updated=datetime.datetime.now(),
    )

    if body.get('mode') == 'computer':
        g.player2 = User.objects.get(id='b70bf23b-d10a-481d-82d5-d228d4b4d289')

    g.save()

    return JsonResponse({
        'gameid': g.id
    })


def addplayer(request):
    body = json.loads(request.body.decode('utf-8'))

    g = Games.objects.get(id=body.get("gameid"))

    if g.player1.id != request.session['id']:
        print(g.player1.id)
        if g.player2 == None:
            g.player2 = User.objects.get(id=request.session['id'])
            print(g.player2.id)
            g.save()
            u = User.objects.get(id=request.session['id'])

    player2 = ''

    if (g.player2):
        player2 = g.player2.id

    return JsonResponse({
        'player2': player2
    })


def turn(request):
    body = json.loads(request.body.decode('utf-8'))
    g = Games.objects.get(id=body.get("gameid"))
    board = g.board
    board = board.split(',')
    player = g.turn
    win = ['']
    valid = ''

    index = body.get('index')

    if request.session['id'] != g.player1.id and request.session['id'] != g.player2.id and g.player2:
        valid = 'The players for this game have already been chosen.'

    else:

        if (player == 'X' and g.player2 and g.player2.id == request.session['id']) or (player == 'O' and g.player1.id == request.session['id']):
            valid = 'Wait your turn!'
            print('invalid')
        elif (player == 'X' and g.player1.id == request.session['id']) or (player == 'O' and g.player2 and g.player2.id == request.session['id']):
            print('valid')
            if board[index] == ' ' and valid == '':
                board[index] = player
                player = nextturn(player)
                win = rules(board, win)
                print(board)
            else:
                valid = 'Invalid move, play again.'

        # Computer takes a turn
        if g.player2 and g.player2.id == 'b70bf23b-d10a-481d-82d5-d228d4b4d289' and player == 'O' and win[0] == '':
            index = random.randrange(9)
            while board[index] != ' ':
                index = random.randrange(9)
            board[index] = player
            player = nextturn(player)
            win = rules(board, win)
            g.turn = player

        # Game has ended
        if win[0] != '':
            player = nextturn(player)

        u1 = User.objects.get(id=g.player1.id)
        if g.player2:
            u2 = User.objects.get(id=g.player2.id)

        if len(win) == 3:
            if player == 'X':
                g.winner = g.player1
                u1.win += 1
                u2.lose += 1
            else:
                g.winner = g.player2
                u2.win += 1
                u1.lose += 1
        elif win[0] == 'draw':
            u1.draw += 1
            u2.draw += 1

        u1.save()
        if g.player2:
            u2.save()

        g.turn = player
        g.board = ",".join(board)
        g.updated = datetime.datetime.now()
        g.save()

    return state(request, g.id, win, valid)


def rules(board, win):
    turns = 0
    for x in board:
        if x != ' ':
            turns += 1

    if board[0] == board[1] and board[1] == board[2] and board[0] != ' ':
        win = [0, 1, 2]
    elif board[3] == board[4] and board[4] == board[5] and board[3] != ' ':
        win = [3, 4, 5]
    elif board[6] == board[7] and board[7] == board[8] and board[6] != ' ':
        win = [6, 7, 8]
    elif board[0] == board[3] and board[3] == board[6] and board[0] != ' ':
        win = [0, 3, 6]
    elif board[1] == board[4] and board[4] == board[7] and board[1] != ' ':
        win = [1, 4, 7]
    elif board[2] == board[5] and board[5] == board[8] and board[2] != ' ':
        win = [2, 5, 8]
    elif board[0] == board[4] and board[4] == board[8] and board[0] != ' ':
        win = [0, 4, 8]
    elif board[2] == board[4] and board[4] == board[6] and board[2] != ' ':
        win = [2, 4, 6]
    elif turns == 9:
        win = ['draw']
    return win


def nextturn(player):
    if player == 'X':
        return 'O'
    else:
        return 'X'
