import React, { useState } from 'react';
import './menu.css';
import axios from 'axios';
import { useHistory } from 'react-router-dom';

const Menu = () => {
    const history = useHistory();

    const [mode, setMode] = useState('computer');

    // Run on submit/click button
    const startGame = event => {
        event.preventDefault();
        axios.post('/api/setgame/', { mode }).then(response => {
            const gameid = response.data.gameid;
            history.push(`/game/${gameid}`);
        });
    };

    return (
        <div>
            <div className="selection">
                <h1>Menu</h1>
            </div>
            <form onSubmit={startGame}>
                <div className="selection">
                    <h1>Game</h1>

                    <select name="game" value={mode} onChange={e => setMode(e.target.value)}>
                        <option value="computer">Player vs Computer</option>
                        <option value="player">Player vs Player</option>
                    </select>
                </div>
                <div className="selection">
                    <input type="submit" value="Play" />
                </div>
            </form>
        </div>
    );
};

export default Menu;
