import React, { useState, useEffect } from 'react';
import './profile.css';
import axios from 'axios';

const Profile = () => {
    const [nickname, setNickname] = useState('');
    const [username, setUsername] = useState('');
    const [win, setwin] = useState('');
    const [lose, setlose] = useState('');
    const [draw, setdraw] = useState('');
    const [total, settotal] = useState('');

    const setuser = event => {
        event.preventDefault();
        axios.post('/api/userinfo/', { nickname }).then(response => {
            setUsername(response.data.username);
        });
    };

    useEffect(() => {
        axios.get('/api/userinfo/').then(response => {
            setUsername(response.data.username);
            setwin(response.data.win);
            setlose(response.data.lose);
            setdraw(response.data.draw);
            settotal(response.data.total);
        });
    }, []);

    return (
        <div>
            <div className="selection">
                <h1>Profile</h1>
            </div>
            <div className="selection">
                <h1>User Name: {username}</h1>
            </div>
            <div className="selection">
                <h1>Won: {win}</h1>
            </div>
            <div className="selection">
                <h1>Lost: {lose}</h1>
            </div>
            <div className="selection">
                <h1>Draw: {draw}</h1>
            </div>
            <div className="selection">
                <h1>Total Number of Games Played: {total}</h1>
            </div>
            <form onSubmit={setuser}>
                <div className="selection">
                    <label>
                        User Name (4 to 8 characters): <input type="text" name="playername" value={nickname} onChange={e => setNickname(e.target.value)} />
                    </label>
                </div>
                <div className="selection">
                    <input type="submit" value="Save" />
                </div>
            </form>
        </div>
    );
};

export default Profile;
