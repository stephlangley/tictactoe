import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css';
import Menu from '../components/menu.js';
import Board from '../components/board.js';
import Profile from '../components/profile.js';
import Games from '../components/games.js';
import axios from 'axios';

const App = () => {
    useEffect(() => {
        axios.post('/api/user/').then(response => {
            console.log(response);
        });
    }, []);

    return (
        <div>
            <div>
                <header className="App App-header">Tic Tac Toe</header>
            </div>
            <Router>
                <div>
                    <nav>
                        <ul>
                            <li>
                                <Link to="/profile">Profile</Link>
                            </li>
                            <li>
                                <Link to="/menu">Menu</Link>
                            </li>
                            <li>
                                <Link to="/game">Game</Link>
                            </li>
                        </ul>
                    </nav>

                    <Switch>
                        <Route path="/menu">
                            <Menu />
                        </Route>
                        <Route path="/game/:id">
                            <Board />
                        </Route>
                        <Route path="/game">
                            <Games />
                        </Route>
                        <Route path="/profile">
                            <Profile />
                        </Route>
                    </Switch>
                </div>
            </Router>
        </div>
    );
};

export default App;
